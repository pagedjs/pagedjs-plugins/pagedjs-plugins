class addContentToBlankPage extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
  }
  afterPageLayout(page) {
    if (page.classList.contains("pagedjs_blank_page")) {
      let newblock = document.createElement("div");
      newblock.classList.add("uke");
      newblock.innerHTML =
        "<p>this text will be added to any blank page</p>";
      page
        .querySelector(".pagedjs_page_content")
        .insertAdjacentElement("afterbegin", newblock);
    }
  }
}

Paged.registerHandlers(addContentToBlankPage);

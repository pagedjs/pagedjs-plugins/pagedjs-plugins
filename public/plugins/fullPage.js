// lets you manualy add classes to some pages elements
// to simulate page floats.
// works only for elements that are not across two pages
//
// check for `--page-float: full-page;` property 

const classElemFullPage = "imgFullPage"; // ← class of full page images

class fullPage extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
    this.floatFullPage;
  }

  onDeclaration(declaration, dItem, dList, rule) {
    if (declaration.property == "--page-float") {
      if (declaration.value.value.includes("full-page")) {
        let sel = csstree.generate(rule.ruleNode.prelude);
        sel = sel.replace('[data-id="', "#");
        sel = sel.replace('"]', "");
        this.floatFullPage = sel.split(",");
      }
    }
  }

  afterParsed(content) {
    if (this.floatFullPage) {
      this.floatFullPage.forEach((elNBlist) => {
        content.querySelectorAll(elNBlist).forEach((el) => {
          el.classList.add("imgFullPage");
        });
      });
    }
  }

  afterPageLayout(page) {
    if (page.querySelector(".imgFullPage")) {
      page.classList.add("fullPage");
      page.querySelector(".imgFullPage").style.display = "none";
      page.style.background = `url(${page.querySelector(".imgFullPage").src})`;
      page.style.backgroundRepeat = `no-repeat`;
      page.style.backgroundSize = `cover`;
      page
        .querySelectorAll(".pagedjs_margin-content")
        .forEach((marginContent) => marginContent.remove());
    }
  }
}

Paged.registerHandlers(fullPage);

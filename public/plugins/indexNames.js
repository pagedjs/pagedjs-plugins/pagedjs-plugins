// Create an index from a list of classes and generate a list of that.
// in this script, we’re looking for .keyword

class index_names extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
    this.index_namesList = new Set();
    this.index_nameIndex = [];
    this.num = 0;
  }

  beforeParsed(content) {
    content.querySelectorAll(".keyword").forEach((term, index) => {
      term.id = slugify(`${term.textContent}_def`);
      term.dataset.key = `key-${index}`;

      term.classList.add("key");
      term.id = slugify(`${term.textContent}_word`);
      term.dataset.key = `key-${index}`;

      let link = document.createElement("a");
      link.innerHTML = term.innerHTML.toLowerCase();
      link.href = `#${slugify(term.textContent)}-word`;

      this.index_nameIndex.push(link);
    });

    let liste = document.createElement("ul");
    let orderKey = this.index_nameIndex.sort();
    for (let key of orderKey) {
      let item = document.createElement("li");
      item.insertAdjacentElement("beforeend", key);
      liste.appendChild(item);
    }
    console.log(liste);
    // create the element that will include the index
    liste.classList.add("indexList");
    console.log(content.querySelector(".index_here"));
    content
      .querySelector(".index_here")
      .insertAdjacentElement("afterend", liste);
    content.querySelector(".index_here").remove();
  }

  renderNode(node, sourceNode) {
    if (node.nodeType == 1) {
      if (node.classList.contains("index_name")) {
        // add the term to the set
        this.index_namesList.add(node);
        // remove the term form the element
        // node.remove();
      }

      let terms = node.querySelectorAll(".index_name");
      if (terms.length) {
        terms.forEach((term) => {
          this.index_namesList.add(term);
          // term.remove();
        });
      }
    }
  }
}

Paged.registerHandlers(index_names);

function slugify(str) {
  str = str.replace(/^\s+|\s+$/g, "");

  // Make the string lowercase
  str = str.toLowerCase();

  // Remove accents, swap ñ for n, etc
  var from =
    "ÁÄÂÀÃÅČÇĆĎÉĚËÈÊẼĔȆÍÌÎÏŇÑÓÖÒÔÕØŘŔŠŤÚŮÜÙÛÝŸŽáäâàãåčçćďéěëèêẽĕȇíìîïňñóöòôõøðřŕšťúůüùûýÿžþÞĐđßÆa·/_,:;";
  var to =
    "AAAAAACCCDEEEEEEEEIIIINNOOOOOORRSTUUUUUYYZaaaaaacccdeeeeeeeeiiiinnooooooorrstuuuuuyyzbBDdBAa------";
  for (var i = 0, l = from.length; i < l; i++) {
    str = str.replace(new RegExp(from.charAt(i), "g"), to.charAt(i));
  }

  // Remove invalid chars
  str = str
    .replace(/[^a-z0-9 -]/g, "")
    // Collapse whitespace and replace by -
    .replace(/\s+/g, "-")
    // Collapse dashes
    .replace(/-+/g, "-");

  return str;
}

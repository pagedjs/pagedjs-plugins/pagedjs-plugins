class removeEmpties extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
  }

  beforeParsed(content) {
    let tags = [];
    content.querySelectorAll("section").forEach((section, sectionIndex) => {
      tags.forEach((tag) => {
        content.querySelectorAll(tag).forEach((el, index) => {
          if (!el.id) {
            el.id = `section-${sectionIndex}-el-${el.tagName.toLowerCase()}-${index}`;
          }
        });
      });
    });
  }
}

Paged.registerHandlers(removeEmpties);

